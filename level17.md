# Command: <br />
1.  functionSignature = {
    name: 'destroy',
    type: 'function',
    inputs: [
        {
            type: 'address',
            name: '_to'
        }
    ]
}
2.  params = [player] <br />
3. data = web3.eth.abi.encodeFunctionCall(functionSignature, params) <br />
4. await web3.eth.sendTransaction({from: player, to: tokenAddr, data})